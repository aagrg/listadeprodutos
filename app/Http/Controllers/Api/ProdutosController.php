<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Produtos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class ProdutosController extends Controller{
    
    public function index(){
        $produtos = Produtos::all();
        return response()->json($produtos);
    }

    public function store(Request $request){

        $validated = Validator::make($request->all(), [
            'nome_produto' => 'required|max:255|min:04',
            'data_cadastro' => 'required',
            'quantidade' => 'required'
        ]);

        if ($validated->fails()) {
            return response()->json([
                'mensagem' => 'Erro! Campos Obrigatórios não preenchidos!'
            ], 400);
        }

        $produtos = Produtos::create($request->all());

        return response()->json([
            'mensagem' => 'Produto cadastrado corretamente!',
            'produto' => $produtos
        ], 201);

    }

    public function show(Produtos $produtos, $id=''){
        $produto = Produtos::find($id); 
        if(!$produto) {
            return response()->json([
                'mensagem'   => 'Produto não encontrado!',
            ], 404);
        }else{
            return response()->json($produto);
        }
    }

    public function update(Request $request, Produtos $produtos, $id=''){
        $validated = Validator::make($request->all(), [
            'nome_produto' => 'required|max:255|min:04',
        ]);

        if ($validated->fails()) {
            return response()->json([
                'mensagem' => 'Erro! Campos Obrigatórios não preenchidos!'
            ], 400);
        }else{
            $produto = Produtos::find($id);
            if(!$produto){
                return response()->json([
                    'mensagem'   => 'Produto Não Encontrado!',
                ], 404);
            }else{
                $campos = $request->all();
                $produto->fill($campos)->save();
                return response()->json($produto);
            }
        }
    }

    public function destroy(Produtos $produtos, $id=''){
        $produto = Produtos::find($id); 
        if(!$produto) {
            return response()->json([
                'mensagem'   => 'Produto não encontrado!',
            ], 404);
        }else{
            $produto->delete();
            return response()->json(['mensagem'=> 'Produto excluído com sucesso!']);
        }    
    }
}
